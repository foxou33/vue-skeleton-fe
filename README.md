# Vue Skeleton FE

Vue.js skeleton frontend application.

[![pipeline status](https://gitlab.com/foxou33/node-skeleton-fe/badges/master/pipeline.svg)](https://gitlab.com/foxou33/node-skeleton-fe/commits/master)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing
purposes. See deployment for notes on how to deploy the project on a live system.

#### Prerequisites

- Node.js 12.14+
- npm 6.13+

## License

Copyright &copy; 2021 foxou33.

This project is licensed under the GNU GPL v3 License - see the [LICENSE](LICENSE) file for details.
